﻿using Objects;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Managers
{
    public class SpawnRollingObjects : MonoBehaviour
    {
        //Editor
        public List<EnemySpawnPoint> spawnPoints;

        void Update()
        {
            foreach (var point in spawnPoints)
                point.InnerUpdate();
        }
    }
}