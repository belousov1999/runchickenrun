﻿using Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UI.Objects;
using UnityEditor;
using UnityEngine;

namespace Managers
{
    public class RaceTracker : MonoBehaviour
    {
        //Private
        private ProgressBar progressBar;
        private float all_dist, _start;
        private bool preloaded = false;

        //Private Static
        private static RaceTracker _instance;

        private void Awake()
        {
            _instance = this;
        }

        private void Start()
        {
            progressBar = UIManager.GetSpecialObject<ProgressBar>(UI.Enums.UiTypeEnum.RaceTracker);
            EventManager.LevelChangedEvent += (obj, args) =>
              {
                  _start = ChickenController.PlayerChicken.transform.position.x;
                  var _end = RaceEndPoint.Instance.transform.position.x;
                  all_dist = _start - _end;
                  preloaded = true;
              };
        }

        public static int GetPlayerPlaceByTrackers()
        {
            var p_progress = _instance.GetProgress(ChickenController.PlayerChicken.transform.position.x);
            var place = 1;
            foreach(var enemy in ChickenController.Stickmans)
            {
                var e_progress = _instance.GetProgress(enemy.transform.position.x);
                if (e_progress > p_progress)
                    place++;
            }
            return place;
        }

        private float GetProgress(float x)
            => (_start - x) / all_dist;

        private void FixedUpdate()
        {
            if (!preloaded) return;
            var p_pos = ChickenController.PlayerChicken.transform.position.x;
            var v = GetProgress(p_pos);
            progressBar.Value = v;
            if (v >= 1f)
                preloaded = false;
        }
    }
}
