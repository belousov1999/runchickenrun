﻿using Objects;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Managers
{
    public class HoldController : MonoBehaviour
    {
        //Public Static
        public static bool Holding { get; private set; }

        void SetHold(bool hold)
        {
            if (GameStateManager.GameState == Enums.GameStateEnum.Start)
                GameStateManager.GameState = Enums.GameStateEnum.Playing;
            Holding = hold;
            ChickenController.PlayerChicken.isRunning = hold;
        }

        void Update()
        {
            if (GameStateManager.GameState != Enums.GameStateEnum.Start &&
                GameStateManager.GameState != Enums.GameStateEnum.Playing) return;

            if (Input.GetMouseButtonDown(0))
                SetHold(true);
            else if (Input.GetMouseButtonUp(0))
                SetHold(false);

            if (Input.touchCount > 0)
            {
                Touch touch = Input.GetTouch(0);

                switch (touch.phase)
                {
                    case TouchPhase.Began:
                        SetHold(true);
                        break;
                    case TouchPhase.Ended:
                        SetHold(false);
                        break;
                }
            }
        }
    }
}