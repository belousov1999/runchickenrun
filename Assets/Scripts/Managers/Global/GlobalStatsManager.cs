﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalStatsManager : MonoBehaviour
{
    //Editor
    public float deathTime;
    public float deathTime_enemy;

    //Public Static
    public static GlobalStatsManager Instance { get; private set; }

    private void Awake()
    {
        Instance = this;
    }
}
