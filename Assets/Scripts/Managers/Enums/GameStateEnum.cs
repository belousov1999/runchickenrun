﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Managers.Enums
{
    public enum GameStateEnum
    {
        GameMenu,
        Start,
        Playing,
        FinishWin,
        FinishLose,
        Loading,
        EditorSettings
    }
}