﻿using Objects;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class StickSpeedFieldSlider : SliderField
{
    private static StickSpeedFieldSlider _instance;

    void Awake()
    {
        _instance = this;
    }

    void Start()
    {
        base.Start();
        
        Value = ChickenController.Stickmans.First().Movespeed.DefaultValue;
    }

    public static void OnChickSpawn()
    {
        if (_instance == null) return;
        ChickenController.Stickmans.ForEach((x) => x.Movespeed.DefaultValue = _instance.Value);
    }

    protected override void OnValueChanged(float f)
    {
        ChickenController.Stickmans.ForEach((x) => x.Movespeed.DefaultValue = f);
    }
}
