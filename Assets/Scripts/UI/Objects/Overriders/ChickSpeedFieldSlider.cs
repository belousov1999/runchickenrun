﻿using Objects;
using Objects.Events;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChickSpeedFieldSlider : SliderField
{
    private static ChickSpeedFieldSlider _instance;

    void Awake()
    {
        _instance = this;
    }

    void Start()
    {
        base.Start();
        Value = ChickenController.PlayerChicken.Movespeed.DefaultValue;
    }

    public static void OnChickSpawn()
    {
        if (_instance == null) return;
        ChickenController.PlayerChicken.Movespeed.DefaultValue = _instance.Value;
    }

    protected override void OnValueChanged(float f)
    {
        ChickenController.PlayerChicken.Movespeed.DefaultValue = f;
    }
}
