﻿using Objects.Controllers;
using Objects.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Objects.Overriders
{
    class SpecialValueFieldSlider : SliderField
    {

        public enum SpecialType
        {
            ForkHeight,
            ForkUpTime,
            ForkDownTime,
            DeathTimePlayer,
            DeathTimeEnemies
        }

        //Public
        public SpecialType fieldType;

        //Private Static
        private static List<SpecialValueFieldSlider> specialSliderList = new List<SpecialValueFieldSlider>();
        private static List<Component> list = new List<Component>();

        void Awake()
        {
            specialSliderList.Add(this);
        }

        void Start()
        {
            base.Start();
            PresetValue();
        }

        private void PresetValue()
        {
            switch (fieldType)
            {
                case SpecialType.ForkHeight:
                case SpecialType.ForkUpTime:
                case SpecialType.ForkDownTime:
                    list.AddRange(FindObjectsOfType<KillStoneController>());
                    break;
            }
            KillStoneController first;
            switch (fieldType)
            {
                case SpecialType.ForkHeight:
                    first = (KillStoneController)list.FirstOrDefault();
                    if (first != null)
                    {
                        Value = first.upDistance;
                    }
                    break;
                case SpecialType.ForkUpTime:
                    first = (KillStoneController)list.FirstOrDefault();
                    if (first != null)
                    {
                        Value = first.moveTimeUp;
                    }
                    break;
                case SpecialType.ForkDownTime:
                    first = (KillStoneController)list.FirstOrDefault();
                    if (first != null)
                    {
                        Value = first.moveTimeDown;
                    }
                    break;
                case SpecialType.DeathTimePlayer:
                    Value = GlobalStatsManager.Instance.deathTime;
                    break;
                case SpecialType.DeathTimeEnemies:
                    Value = GlobalStatsManager.Instance.deathTime_enemy;
                    break;
            }
        }

        public static void OnLevelLoaded()
        {
            foreach (var slider in specialSliderList)
            {
                slider.OnLevelLoadedInner();
            }
        }

        private void OnLevelLoadedInner()
        {
            if (Value == 0)
                PresetValue();
            switch (fieldType)
            {
                case SpecialType.ForkHeight:
                case SpecialType.ForkUpTime:
                case SpecialType.ForkDownTime:
                    list.Clear();
                    list.AddRange(FindObjectsOfType<KillStoneController>());
                    OnValueChanged(Value);
                    break;
                case SpecialType.DeathTimePlayer:
                case SpecialType.DeathTimeEnemies:
                    OnValueChanged(Value);
                    break;
            }

            ChickenController.PlayerChicken.Movespeed.DefaultValue = Value;
        }


        protected override void OnValueChanged(float f)
        {
            switch (fieldType)
            {
                case SpecialType.ForkHeight:
                    list.ForEach((x) => ((KillStoneController)x).upDistance = f);
                    break;
                case SpecialType.ForkUpTime:
                    list.ForEach((x) => ((KillStoneController)x).moveTimeUp = f);
                    break;
                case SpecialType.ForkDownTime:
                    list.ForEach((x) => ((KillStoneController)x).moveTimeDown = f);
                    break;
                case SpecialType.DeathTimePlayer:
                    GlobalStatsManager.Instance.deathTime = Value;
                    ChickenController.PlayerChicken.DeathTime = Value;
                    break;
                case SpecialType.DeathTimeEnemies:
                    GlobalStatsManager.Instance.deathTime_enemy = Value;
                    ChickenController.Stickmans.ForEach((x) => x.DeathTime = Value);
                    break;
            }
        }
    }
}
