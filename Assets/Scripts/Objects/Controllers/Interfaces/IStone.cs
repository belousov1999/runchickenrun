﻿using Objects;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IStone 
{
    bool IsInStoneRadius();

    float GetMovespeedMultiplier();

    Vector3 GetScaleMultiplier();

    Vector3 GetPosition();

    StoneTypes GetStoneType();

    Transform GetBackPoint();

    float GetStoneHeight();

    bool HasSpecialEvent();

    bool IsOnlyForEnemy();
}
