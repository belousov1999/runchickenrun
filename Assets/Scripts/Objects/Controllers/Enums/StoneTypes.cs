﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum StoneTypes 
{
    Jump,
    Crawl,
    FallingStone,
    SpeedUp
}
