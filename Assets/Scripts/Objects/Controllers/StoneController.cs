﻿using Objects;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoneController : MonoBehaviour, IStone
{
    //Editor
    public StoneTypes stoneType;
    [Range(0f, 5f)]
    public float speedMult;
    public Vector3 scaleMult = Vector3.one;
    public Transform backPoint;
    public float stoneHeight = 1f;
    public bool hasSpecialEvent;
    public bool isOnlyForEnemy = false;

    //Private
    private bool inStoneRadius;

    //IStone
    public float GetMovespeedMultiplier()
        => speedMult;

    public Vector3 GetScaleMultiplier()
        => scaleMult;

    public Vector3 GetPosition()
        => transform.position + Vector3.left;

    public bool IsInStoneRadius()
        => inStoneRadius;


    public StoneTypes GetStoneType()
        => stoneType;

    public Transform GetBackPoint()
        => backPoint;

    public float GetStoneHeight()
        => stoneHeight;

    public bool HasSpecialEvent()
        => hasSpecialEvent;


    private void OnTriggerEnter(Collider other)
    {
        if (!inStoneRadius && other.gameObject.CompareTag("Player"))
        {
            inStoneRadius = true;
            lastChicken = other.transform.root.gameObject.GetComponent<ChickenController>();
            lastChicken.NearStone = this;
        }
    }

    private ChickenController lastChicken;
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            if(lastChicken==null)
                lastChicken = other.transform.root.gameObject.GetComponent<ChickenController>();
            if((StoneController)lastChicken.NearStone != this)
                lastChicken.NearStone = this;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (inStoneRadius && other.gameObject.CompareTag("Player"))
        {
            inStoneRadius = false;
            lastChicken.NearStone = null;
        }
    }

    public bool IsOnlyForEnemy()
        => isOnlyForEnemy;
}
