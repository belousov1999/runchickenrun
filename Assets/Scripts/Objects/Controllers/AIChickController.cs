﻿using Managers.Enums;
using Objects;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ChickenController))]
public class AIChickController : MonoBehaviour
{
    //Public
    public float logicDelay = 3f;

    //Test
    [Header("Test")]
    [SerializeField] private bool isRun;

    //Private
    private ChickenController chickController;

    private void Start()
    {
        chickController = GetComponent<ChickenController>();
    }


    float wait_t;
    void Update()
    {
        if (chickController.NearStone != null
            && chickController.NearStone.HasSpecialEvent()
            && chickController.NearStone.GetPosition().x <= transform.position.x)
        {
            wait_t += Time.deltaTime;
            if (wait_t > 1.5f)
            {
                wait_t = 0;
                chickController.isRunning = false;
                isRun = false;
            }
        }
        else
        {
            chickController.isRunning = true;
            isRun = true;
        }
    }
}
