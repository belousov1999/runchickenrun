﻿using Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Objects.Controllers
{
    public class KillStoneController : MonoBehaviour
    {
        //Public 
        public float upDistance = 1f;
        public float moveTimeUp;
        public float moveTimeDown;
        public float moveWaitTime;
        public Transform respawn_point;

        //Private
        private bool isFalling = false, isWaiting = false;
        private Vector3 startPos, upPos;
        private Collider col;

        private void Start()
        {
            col = GetComponent<BoxCollider>();
            startPos = transform.position;
            upPos = startPos + Vector3.up * upDistance;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (!isFalling || !other.gameObject.CompareTag("Player")) return;
            var controller = other.gameObject.GetComponent<ChickenController>();
            if (controller != null)
            {
                controller.transform.position = respawn_point.position;
                CameraController.Instance.UpdateTargetCameraPos();
                controller.KillChicken();
            }
        }

        private float t = 0;
        private void Update()
        {
            if (isWaiting)
            {
                t += Time.deltaTime;
                if (t >= moveWaitTime)
                {
                    t = 0;
                    isWaiting = false;
                }
                else
                    return;
            }

            if (!isFalling)
            {
                t += Time.deltaTime;
                transform.position = Vector3.Lerp(startPos, upPos, t / moveTimeUp);
                if (t >= moveTimeUp)
                {
                    t = 0;
                    isFalling = true;
                    isWaiting = true;
                    col.isTrigger = true;
                }
            }
            else
            {
                t += Time.deltaTime;
                transform.position = Vector3.Lerp(upPos, startPos, t / (moveTimeDown));
                if (t >= moveTimeDown)
                {
                    t = 0;
                    isFalling = false;
                    isWaiting = true;
                    col.isTrigger = false;
                }
            }
        }
    }
}
