﻿using Objects;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StickmanController : MonoBehaviour
{
    //Private
    private Animator _animator;
    private float fall_t;
    private bool falling, moving;
    private Transform chikenTrans;
    private float _startDist,_distance, _target_X_Position;

    //Private Const
    private const float Movespeed = 2f;

    void Start()
    {
        _animator = GetComponent<Animator>();
        chikenTrans = ChickenController.PlayerChicken.transform;
        _distance = chikenTrans.position.x - transform.position.x;
        _startDist = _distance;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!other.gameObject.CompareTag("Rolling")) return;
        _animator.SetBool("fall", true);
        fall_t = 1f;
        falling = true;
    }

    void Update()
    {
        if (fall_t > 0)
        {
            fall_t -= Time.deltaTime;
            if (fall_t <= 0)
            {
                _animator.SetBool("fall", false);
                falling = false;
            }
        }
        if (!falling)
        {
            if (chikenTrans.position.x - transform.position.x >= _distance)
                moving = false;
            else
            {
                if (moving == false)
                    _distance = Random.Range(_startDist - 0.5f, _startDist + 1f);
                moving = true;
                transform.position = transform.position + Vector3.left * Movespeed * Time.deltaTime;
            }
            _animator.SetBool("walk", moving);
        }
    }
}
