﻿using Exploder;
using Exploder.Utils;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms;

namespace Objects
{
    public class RollingObject : MonoBehaviour
    {
        //Public
        public float rollingForceOffset = 1f;
        public bool notAutoFly;
        public bool notFixedRolling;

        //Private
        private Rigidbody _rb;
        private bool _destroyed, _exploded, _flyingInCamera, _precached;
        private Vector3 cameraPos;

        private void Awake()
        {
            ExploderSingleton.Instance?.CrackObject(this.gameObject, (_, __) =>
            {
                _precached = true;
            });
        }

        void Start()
        {
            gameObject.layer = 9; //Rolling
            gameObject.tag = "Rolling";
            _rb = GetComponent<Rigidbody>();
        }

        public void OnContactWithPlayerChicken()
        {
            if (notAutoFly) return;
            cameraPos = Camera.main.transform.position - Vector3.up;
            cameraPos.x = transform.position.x;
            _rb.isKinematic = true;
            _flyingInCamera = true;
        }

        private void OnCollisionEnter(Collision collision)
        {
            if (!collision.gameObject.CompareTag("Barrier")) return;
            // DestroyGO();
            if (!_destroyed && !_exploded)
            {
                _exploded = true;
                if (_precached)
                    ExploderSingleton.Instance?.ExplodeCracked(this.gameObject);
                else
                    ExploderSingleton.Instance?.ExplodeObject(this.gameObject);

                Destroy(this.gameObject, 0.5f);
            }
        }

        private float tramp_t;
        private void OnCollisionStay(Collision collision)
        {
            if (!collision.gameObject.CompareTag("Mountain") && !collision.gameObject.CompareTag("trampline"))
                return;
            var mult = 1f;
            if (collision.gameObject.CompareTag("trampline"))
            {
                tramp_t += Time.deltaTime;

                mult = Mathf.Lerp(1f, 25f, tramp_t);
            }
            else
                tramp_t = 0;
            _rb.AddForce((Vector3.forward + transform.forward).normalized * rollingForceOffset * mult);
        }

        void DestroyGO()
        {
            if (!_destroyed)
            {
                Destroy(this.gameObject);
                _destroyed = true;
            }
        }

        private void FixedUpdate()
        {
            if (notFixedRolling) return;
            if (_rb.velocity.z < 0.5f)
                _rb.AddForce(transform.forward * rollingForceOffset * 5f);
        }

        void Update()
        {
            if (_rb.velocity.y < -15f)
                DestroyGO();
            if (_flyingInCamera)
            {
                transform.position = Vector3.MoveTowards(transform.position, cameraPos, 15f * Time.deltaTime);
                if (Vector3.Distance(transform.position, cameraPos) < 1f)
                    DestroyGO();
            }
        }
    }
}