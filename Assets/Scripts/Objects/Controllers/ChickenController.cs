﻿using Managers;
using Objects.Controllers.Enums;
using Objects.Overriders;
using System.Collections;
using System.Collections.Generic;
using UI.Enums;
using UI.Managers;
using UnityEngine;
using UnityEngine.UI;

namespace Objects
{
    public class ChickenController : MonoBehaviour
    {
        //Editor
        public ChickenType chickType;
        public bool isPlayerChicken;
        public float chickenSpeed;


        //Testing
        [Header("Testing")]
        [SerializeField] private bool isRunning1 = true;
        [SerializeField] private bool InStoneRadius;
        [SerializeField] private bool Isstoning;
        [SerializeField] private float _testMovespeed;

        //Public
        public StatValue<float> Movespeed;
        public StatValue<Vector3> Scale;
        public float DeathTime { get; set; }

        public IStone NearStone
        {
            get => nearStone; set
            {
                InStoneRadius = value != null;

                nearStone = value;
                if (value != null)
                    lastStone = value;
                OnStoneChanged(isRunning);
            }
        }
        public bool isRunning
        {
            get => isRunning1; set
            {
                isRunning1 = value;
                // if (isPlayerChicken)
                OnStoneChanged(value);
            }
        }

        //Private
        private Animator _animator;
        private Rigidbody _rb;
        private float falling_t, finishStop_t, _stoning_t, _y_ground, _stoneHeight;
        private bool fall, finished, dancing, _stoning, _jumping, _scaling, _autoRun;
        private IStone nearStone, lastStone;
        private Vector3 _jumpPoint, _jumpStartPos, _jumpEndPos, bending;
        private Vector3 deathPoint = Vector3.zero, camPoint = Vector3.zero; //Player Death vars
        private float _jumpTime, _scale_t, _zpos;
        private const int lineSteps = 10;
        private const float scaleTime = 0.2f;
        private Quaternion _debug_rot;

        //Private Static
        private static bool clearSetup;
        private static List<string> idleNames = new List<string> { "Idle A", "Idle B", "Idle C" };

        //Public Static
        public static ChickenController PlayerChicken { get; private set; }
        public static List<ChickenController> Stickmans { get; private set; } = new List<ChickenController>();

        void Awake()
        {
            if (isPlayerChicken)
                PlayerChicken = this;
            Movespeed = new StatValue<float>(chickenSpeed);
            Scale = new StatValue<Vector3>(transform.localScale);
            _zpos = transform.position.z;
            _debug_rot = transform.rotation;
        }

        void Start()
        {
            _animator = GetComponent<Animator>();
            _rb = GetComponent<Rigidbody>();
            if (isPlayerChicken)
                DeathTime = GlobalStatsManager.Instance.deathTime;
            else
                DeathTime = GlobalStatsManager.Instance.deathTime_enemy;

            //Developer Menu
            if (isPlayerChicken)
                ChickSpeedFieldSlider.OnChickSpawn();
            SpecialValueFieldSlider.OnLevelLoaded();

            if (!clearSetup)
            {
                EventManager.OnGameStateChangedEvent += (obj, args) =>
                {
                    if (args.NewGameState == Managers.Enums.GameStateEnum.Start)
                    {
                        foreach (var controller in Stickmans)
                            EventManager.OnGameStateChangedEvent -= controller.AddStickman;
                        Stickmans.Clear();
                    }
                };
                clearSetup = true;
            }
            EventManager.OnGameStateChangedEvent += AddStickman;
        }

        public void KillChicken()
        {
            _animator.SetBool("Roll", true);
            falling_t = 0;
            fall = true;
        }

        private void AddStickman(object _, Events.OnGameStateChangedArgs args)
        {
            if (args.NewGameState == Managers.Enums.GameStateEnum.Playing)
            {
                Stickmans.Add(this);
                StickSpeedFieldSlider.OnChickSpawn();
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.CompareTag("FinishGround"))
            {
                if (isPlayerChicken)
                    Debug.Log("WIN!");
                finished = true;
                return;
            }

            if (!other.gameObject.CompareTag("Rolling") || _jumping || invul_t > 0f || fall) return;
            KillChicken();
            if (PlayerChicken)
                other.gameObject.GetComponent<RollingObject>().OnContactWithPlayerChicken();
        }

        void SetIdle()
        {
            var r = Random.Range(0, 3);
            for (var i = 0; i < idleNames.Count; i++)
                _animator.SetBool(idleNames[i], r == i);
        }

        void OnStoneChanged(bool hold)
        {
            if (_jumping) return;
            if (NearStone != null && NearStone.IsInStoneRadius()) //&& !_autoRun)
            {
                if (NearStone.IsOnlyForEnemy() && isPlayerChicken) return;
                switch (NearStone.GetStoneType())
                {
                    case StoneTypes.Crawl:
                    case StoneTypes.Jump:
                        if (hold)
                            return;
                        break;
                    default:
                        break;
                }
                _stoning = true;
                _stoning_t = 0.3f;
                switch (NearStone.GetStoneType())
                {
                    case StoneTypes.Jump:
                        _animator.SetBool("Run", false);
                        _jumpTime = 0;
                        var backPos = NearStone.GetBackPoint().position;
                        _jumpStartPos = transform.position;
                        _jumpEndPos = new Vector3(backPos.x, _jumpStartPos.y, backPos.z);
                        bending = new Vector3(0, NearStone.GetStoneHeight(), 0);
                        _jumping = true;
                        break;
                    case StoneTypes.Crawl:
                        _autoRun = true;
                        break;
                }
                //Scale modifing
                if (!_scaling)
                {
                    Scale.DefaultValue = Scale.InitialValue;   //Animation prev value
                    var v = Vector3.Scale(Scale.InitialValue, NearStone.GetScaleMultiplier());
                    if (Scale.ModifiedValue != v)
                    {
                        Scale.OnNotStoningOnly = false;
                        Scale.ModifiedValue = v;   //Animation target value
                        _scaling = true;
                    }
                }

                //Speed Modifing
                Movespeed.ModifiedValue = (Movespeed.DefaultValue * NearStone.GetMovespeedMultiplier());
            }
            else if (NearStone == null)
            {
                _stoning = false;
                Scale.DefaultValue = transform.localScale;  //Animation prev value
                Scale.ModifiedValue = Vector3.one; //Animation target value
                Scale.OnNotStoningOnly = true;
                _scaling = true;
                Movespeed.RemoveModified();
                _autoRun = false;
            }
        }

        private void ShowSpecialText()
        {
            if (!isPlayerChicken) return;
            var b = NearStone != null && NearStone.IsInStoneRadius() && NearStone.HasSpecialEvent() && !_jumping && !_autoRun;
            HoldTextManager.UpdateShowing(b);
        }

        private float invul_t;
        private void PlayerDeathAnimation()
        {
            invul_t = 0.7f;
            falling_t += Time.deltaTime;

            if (falling_t <= DeathTime / 2f)
            {
                if (deathPoint == Vector3.zero)
                {
                    _rb.isKinematic = true;
                    deathPoint = transform.position;
                    camPoint = Camera.main.transform.position - Vector3.up * 5f;
                    camPoint.x = transform.position.x;
                }
                transform.position = Vector3.Lerp(deathPoint, camPoint, (falling_t / DeathTime) * 2f);
            }
            else
                transform.position = Vector3.Lerp(deathPoint + Vector3.up * 10f, deathPoint, (falling_t - DeathTime / 2f) / (DeathTime / 2f));

            if (falling_t >= DeathTime)
            {

                deathPoint = Vector3.zero;
                camPoint = Vector3.zero;
                falling_t = 0f;
                fall = false;
                _rb.isKinematic = false;
            }
        }

        void UpdateRacePlaceText()
        {
            var text = UIManager.GetSpecialObject<Text>(UiTypeEnum.RacePlaceText);
            text.text = RaceTracker.GetPlayerPlaceByTrackers() + "";
        }

        private float debug_pos_t;
        void FixedUpdate()
        {
            if (GameStateManager.GameState != Managers.Enums.GameStateEnum.Playing)
            {
                if (dancing)
                {
                    isRunning1 = false;
                    _animator.SetBool("Spin", true);
                }
                return;
            }

            if (!_rb.isKinematic && !_jumping && !fall)
            {
                debug_pos_t += Time.deltaTime;
                if (debug_pos_t >= 0.2f)
                {
                    debug_pos_t = 0;
                    var pos = transform.position;
                    pos.z = _zpos;
                    transform.position = pos;
                    transform.rotation = _debug_rot;
                }
            }

            ShowSpecialText();

            if (invul_t > 0)
            {
                invul_t -= Time.deltaTime;
            }

            Isstoning = NearStone != null && NearStone.HasSpecialEvent();

            if (_scaling
                && (!Scale.OnNotStoningOnly || (Scale.OnNotStoningOnly && !Isstoning)))
            {
                _scale_t += Time.deltaTime;
                if (_scale_t >= scaleTime)
                {
                    _scaling = false;
                    _scale_t = 0;
                }
                else
                    transform.localScale = Vector3.Lerp(Scale.DefaultValue, Scale.ModifiedValue, _scale_t / scaleTime);
            }

            if (_stoning && NearStone == null)
            {
                if (_stoning_t > 0)
                    _stoning_t -= Time.deltaTime;
                isRunning1 = true;
                if (_stoning_t <= 0f)
                {
                    if (!HoldController.Holding)
                        isRunning1 = false;
                    OnStoneChanged(true);
                }
            }

            if (finished && !fall)
            {
                finishStop_t += Time.deltaTime;
                if (finishStop_t >= 1f)
                {
                    isRunning1 = false;
                    _animator.SetTrigger("Spin");
                    dancing = true;
                    UpdateRacePlaceText();
                    if (isPlayerChicken)
                        GameStateManager.GameState = Managers.Enums.GameStateEnum.FinishWin;
                    else
                        GameStateManager.GameState = Managers.Enums.GameStateEnum.FinishLose;
                    Debug.Log("Finished");
                    return;
                }
            }
            if (fall)
            {
                //if (isPlayerChicken)
                PlayerDeathAnimation();

            }
            else if (_jumping)
            {
                if (chickType == ChickenType.chick)
                    _animator.SetBool("Fly", true);
                else if (chickType == ChickenType.cow)
                    _animator.SetBool("Swim", true);
                float timeToTravel = 0.7f;
                _jumpTime += Time.deltaTime;
                var t = _jumpTime / timeToTravel;
                var currentPos = Vector3.Lerp(_jumpStartPos, _jumpEndPos, t);

                var mat_t = Mathf.Sin(Mathf.Clamp01(t) * Mathf.PI);
                currentPos.x += bending.x * mat_t;
                currentPos.y += bending.y * mat_t;
                currentPos.z += bending.z * mat_t;

                transform.position = currentPos;
                if (_jumpTime >= timeToTravel)
                {
                    _jumping = false;
                    _animator.SetBool("Run", true);
                }
            }
            else if (isRunning || _autoRun)
            {
                _testMovespeed = Movespeed.ModifiedValue;
                _animator.SetBool("Run", true);
                transform.position = transform.position + Vector3.left * Movespeed.ModifiedValue * Time.deltaTime;
                //transform.position  = Vector3.MoveTowards(transform.position, transform.position + Vector3.left, Movespeed.ModifiedValue * Time.fixedDeltaTime);
            }
            else if (!finished)
            {
                SetIdle();
            }
        }
    }
}