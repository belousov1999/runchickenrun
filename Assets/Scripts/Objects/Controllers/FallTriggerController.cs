﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallTriggerController : MonoBehaviour
{
    //Editor
    public Rigidbody falling_rb;
    public bool resetParent;

    //Private
    private bool falled;

    private void OnTriggerEnter(Collider other)
    {
        if (falled || !other.gameObject.CompareTag("Player")) return;
        falling_rb.isKinematic = false;
        falled = true;
        if (resetParent)
            falling_rb.transform.SetParent(null);
    }
}
