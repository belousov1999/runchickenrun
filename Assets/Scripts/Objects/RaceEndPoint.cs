﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Objects
{
    public class RaceEndPoint:MonoBehaviour
    {
        public static RaceEndPoint Instance { get; private set; }

        void Awake()
        {
            Instance = this;
        }
    }
}
