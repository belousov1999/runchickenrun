﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Objects
{
    public class StatValue<T> where T : struct
    {
        public T InitialValue { get; private set; }
        public T DefaultValue { get; set; }
        public T ModifiedValue { get; set; }
        public bool OnNotStoningOnly { get; set; }

        public StatValue(T _defaultValue)
        {
            InitialValue = _defaultValue;
            DefaultValue = _defaultValue;
        }

        public void RemoveModified()
            => ModifiedValue = DefaultValue;

        /*
        public void UpdateDefaultValue(T value)
        {
            
            //if (_defaultValue is float)
            //{
            //    var calc = Convert.ToSingle(_sumValue) + (Convert.ToSingle(_defaultValue) - Convert.ToSingle(value));
            //    UpdateModifiedValue((T)Convert.ChangeType(calc, typeof(T)));
            //}
            
            _defaultValue = value;
        }
        */
    }
}
