﻿using Objects;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawnPoint : MonoBehaviour
{
    //Editor
    public float enemySpeed = 5000f;
    public float spawnBetween = 3f;
    public int spawnCount = 1;
    public float multipleSpawnTime = 1f;
    public GameObject prefab;
    public GameObject spawnAnimation;
    public GameObject lookAtPoint;

    //Private
    private float _spawnT, _multSpawnT;
    private bool multipleSpawn;
    private int multSpawned;
    private Vector3 spawnAnimStartPoint, targetPoint;

    private void Start()
    {
        if (spawnAnimation != null)
        {
            spawnAnimStartPoint = spawnAnimation.transform.position;
            targetPoint = spawnAnimStartPoint + new Vector3(0, 1.5f, 0);
        }

    }

    public void InnerUpdate()
    {
        _spawnT -= Time.deltaTime;
        if (spawnAnimation != null)
        {
            if (_spawnT >= spawnBetween / 2f)
                spawnAnimation.transform.position = Vector3.Lerp(spawnAnimStartPoint, targetPoint, (spawnBetween - _spawnT) / (spawnBetween / 2f));
            else
                spawnAnimation.transform.position = Vector3.Lerp(targetPoint, spawnAnimStartPoint, ((spawnBetween / 2f) - _spawnT) / (spawnBetween / 2f));

        }
        if (_spawnT > 0 && !multipleSpawn) return;
        _spawnT = spawnBetween;

        if (spawnCount > 1)
        {
            multipleSpawn = true;
            _multSpawnT -= Time.deltaTime;
            if (_multSpawnT > 0) return;
            _multSpawnT = multipleSpawnTime;
            multSpawned++;
            Spawn();
            if (multSpawned == spawnCount)
            {
                multSpawned = 0;
                _multSpawnT = 0;
                multipleSpawn = false;
            }
        }
        else
            Spawn();
    }

    private void Spawn()
    {
        var rollingObject = Instantiate(prefab, transform.position, prefab.transform.rotation);
        rollingObject.AddComponent<RollingObject>().rollingForceOffset = enemySpeed;
        if (lookAtPoint != null)
        {
            rollingObject.transform.LookAt(lookAtPoint.transform);
            var angle_vector = rollingObject.transform.localEulerAngles;
            rollingObject.transform.localEulerAngles = new Vector3(0, angle_vector.y, 90);
        }
    }
}
